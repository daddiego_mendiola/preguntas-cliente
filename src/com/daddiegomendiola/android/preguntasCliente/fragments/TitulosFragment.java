/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daddiegomendiola.android.preguntasCliente.fragments;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.daddiegomendiola.android.preguntasCliente.Configuracion;
import com.daddiegomendiola.android.preguntasCliente.R;
import com.daddiegomendiola.android.preguntasCliente.restClient.RestClient;

public class TitulosFragment extends ListRestFragment implements
		OnItemClickListener {

	private final String url = Configuracion.SERVIDOR + "preguntas/";
	public static final int NO_TITLE_SELECTED = -1;

	private static final String TAG = TitulosFragment.class.getName();

	// The list of headlines that we are displaying
	List<String> listaTitulos = new ArrayList<String>();
	List<Integer> listaIds = new ArrayList<Integer>();

	// The list adapter for the list we are displaying
	ArrayAdapter<String> mListAdapter;

	// The listener we are to notify when a headline is selected
	OnHeadlineSelectedListener mHeadlineSelectedListener = null;

	/**
	 * Represents a listener that will be notified of headline selections.
	 */
	public interface OnHeadlineSelectedListener {
		/**
		 * Called when a given headline is selected.
		 * 
		 * @param index
		 *            the index of the selected headline.
		 */
		public void onHeadlineSelected(int index);

		public void onTitulosCargados(int index);
	}

	/**
	 * Default constructor required by framework.
	 */
	public TitulosFragment() {
		super();
	}

	@Override
	public void onStart() {
		super.onStart();
		setListAdapter(mListAdapter);
		getListView().setOnItemClickListener(this);
		cargarPreguntas();
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mListAdapter = new ArrayAdapter<String>(getActivity(),
				R.layout.headline_item, listaTitulos);
	}

	/**
	 * Sets the listener that should be notified of headline selection events.
	 * 
	 * @param listener
	 *            the listener to notify.
	 */
	public void setOnHeadlineSelectedListener(
			OnHeadlineSelectedListener listener) {
		mHeadlineSelectedListener = listener;
	}

	/**
	 * Load and display the headlines for the given news category.
	 * 
	 * @param categoryIndex
	 *            the index of the news category to display.
	 */
	public void cargarPreguntas() {
		listaTitulos.clear();
		listaIds.clear();
		Intent restRequest = new Intent(getActivity(), RestClient.class);
		restRequest.setData(Uri.parse(url));
		restRequest.putExtra(RestClient.EXTRA_RESULT_RECEIVER,
				getResultReceiver());
		Log.e(TAG, "Arrancando consulta.");
		getActivity().startService(restRequest);
	}

	public int getNewestQuestionIndex() {
		return this.listaIds.get(0);
	}

	/**
	 * Handles a click on a headline.
	 * 
	 * This causes the configured listener to be notified that a headline was
	 * selected.
	 */
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		if (null != mHeadlineSelectedListener) {
			if (!this.listaIds.isEmpty()) {
				int preguntaid = this.listaIds.get(position);
				mHeadlineSelectedListener.onHeadlineSelected(preguntaid);
			}
		}
	}

	@Override
	public void setSelection(int index) {
		int realIndex = this.listaIds.indexOf(index);
		super.setSelection(realIndex);
	}

	/**
	 * Sets choice mode for the list
	 * 
	 * @param selectable
	 *            whether list is to be selectable.
	 */
	public void setSelectable(boolean selectable) {
		if (selectable) {
			getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
		} else {
			getListView().setChoiceMode(ListView.CHOICE_MODE_NONE);
		}
	}

	private void parseJSON(JSONArray preguntas) throws JSONException {
		this.listaIds.clear();
		this.listaTitulos.clear();
		for (int i = 0; i < preguntas.length(); i++) {
			JSONObject pregunta = preguntas.getJSONObject(i);
			String resp = (Integer.parseInt(pregunta.getString("cantidad_respuestas")) == 1) ? " respuesta." : " respuestas.";
			listaTitulos.add(pregunta.getString("titulo") + " | " + pregunta.getString("cantidad_respuestas") + resp);
			listaIds.add(pregunta.getInt("id"));
		}
	}

	@Override
	public void onRESTResult(int code, String result) {
		if (code == 200) {
			try {
				JSONArray preguntas = new JSONArray(result);
				if (preguntas.length() > 0) {
					parseJSON(preguntas);
					this.mHeadlineSelectedListener.onTitulosCargados(this
							.getNewestQuestionIndex());
				} else {
					this.listaTitulos.add("No hay preguntas!");
				}
			} catch (JSONException e) {
				Log.e(TAG, "Failed to parse JSON.", e);
			}
		}
		mListAdapter.notifyDataSetChanged();
	}
}
