/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daddiegomendiola.android.preguntasCliente.fragments;

import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.daddiegomendiola.android.preguntasCliente.Configuracion;
import com.daddiegomendiola.android.preguntasCliente.activities.VerPerfilActivity;
import com.daddiegomendiola.android.preguntasCliente.restClient.RestClient;
import com.daddiegomendiola.android.preguntasCliente.views.PreguntaWebView;

/**
 * Fragment that displays a news article.
 */
public class PreguntaFragment extends RestFragment {
	private final String url = Configuracion.SERVIDOR + "preguntas/";
	private static final String TAG = PreguntaFragment.class.getName();
	private static final int LOADING_QUESTION = 0;
	private static final int LOADING_ANSWERS = 1;
	private static final int END_LOADING = 2;

	// The webview where we display the article (our only view)
	private PreguntaWebView mWebView;
	private int preguntaIndex;
	private int state = 0;

	// Parameterless constructor is needed by framework
	public PreguntaFragment() {
		super();
	}

	/**
	 * Sets up the UI. It consists of a WebView.
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		this.getActivity().setTitle("Cargando...");
		mWebView = new PreguntaWebView(this.getActivity(), this);
		return mWebView;
	}

	private void reset() {
		this.state = 0;
		this.mWebView.init();
	}

	/**
	 * Displays a particular article.
	 * 
	 * @param article
	 *            the article to display
	 */
	public void mostrarPregunta(int index) {
		this.reset();
		this.preguntaIndex = index;
		Intent restRequest = new Intent(getActivity(), RestClient.class);
		String newurl = url + String.valueOf(index) + "/";
		restRequest.setData(Uri.parse(newurl));
		restRequest.putExtra(RestClient.EXTRA_RESULT_RECEIVER,
				this.getResultReceiver());
		getActivity().startService(restRequest);
	}

	private void mostrarRespuestas() {
		Intent restRequest = new Intent(getActivity(), RestClient.class);
		String newurl = url + String.valueOf(this.preguntaIndex)
				+ "/respuestas/";
		restRequest.setData(Uri.parse(newurl));
		restRequest.putExtra(RestClient.EXTRA_RESULT_RECEIVER,
				this.getResultReceiver());
		getActivity().startService(restRequest);
	}

	private void parsearPregunta(String json) {
		try {
			JSONObject pregunta = new JSONObject(json);
			JSONObject alumno = pregunta.getJSONObject("alumno");
			this.getActivity().setTitle(pregunta.getString("titulo"));
			this.mWebView.procesarPregunta(pregunta, alumno);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
	}

	private void parsearRespuestas(String json) {
		Log.e(TAG, "parseando respuesta-metodo");
		try {
			JSONArray respuestas = new JSONArray(json);
			for (int i = 0; i < respuestas.length(); i++) {
				JSONObject respuesta = respuestas.getJSONObject(i);
				JSONObject alumno = respuesta.getJSONObject("alumno");
				this.mWebView.procesarRespuesta(respuesta, alumno);
			}
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
	}

	private void mostrarFormularioRespuesta() {
		this.mWebView.crearFormularioRespuesta();
	}

	private void showData() {
		this.mWebView.showData();
	}

	// M�todos llamados por el CustomWebView.

	public void responder(String respuesta) {
		Intent restRequest = new Intent(getActivity(), RestClient.class);
		String urlRespuesta = url + String.valueOf(this.preguntaIndex)
				+ "/respuestas/";
		restRequest.setData(Uri.parse(urlRespuesta));
		restRequest.putExtra(RestClient.EXTRA_HTTP_VERB, RestClient.POST);
		restRequest.putExtra(RestClient.EXTRA_RESULT_RECEIVER,
				this.getResultReceiver());
		Bundle params = new Bundle();
		params.putString("descripcion", respuesta);
		restRequest.putExtra(RestClient.EXTRA_PARAMS, params);
		getActivity().startService(restRequest);
	}
	
	public void perfil(int index){
		Intent perfilIntent = new Intent(getActivity(), VerPerfilActivity.class);
		perfilIntent.putExtra(Configuracion.ALUMNO_INDEX, index);
		startActivity(perfilIntent);
	}

	@Override
	public void onRESTResult(int code, String result) {
		Log.e(TAG, String.valueOf(this.state));

		switch (code) {
			case 200:
				switch (this.state) {
					case LOADING_QUESTION:
						Log.e(TAG, "parseando pregunta");
						this.parsearPregunta(result);
						this.state = this.state + 1;
						this.mostrarRespuestas();
						break;
		
					case LOADING_ANSWERS:
						Log.e(TAG, "parseando respuesta");
						this.parsearRespuestas(result);
						this.state = this.state + 1;
						break;
		
					default:
						this.state = this.state + 1;
						break;
					}
					if (this.state == END_LOADING) {
						this.mostrarFormularioRespuesta();
						this.showData();
					}
					break;
	
			case 201:
				this.mostrarPregunta(this.preguntaIndex);
				break;
	
			default:
				break;
		}
	}
}
