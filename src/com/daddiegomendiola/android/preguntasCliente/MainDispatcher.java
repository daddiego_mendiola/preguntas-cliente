package com.daddiegomendiola.android.preguntasCliente;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.daddiegomendiola.android.preguntasCliente.activities.LoginActivity;
import com.daddiegomendiola.android.preguntasCliente.activities.LectorPreguntasActivity;
import com.daddiegomendiola.android.preguntasCliente.activities.RestActivity;
import com.daddiegomendiola.android.preguntasCliente.restClient.RestClient;
import com.daddiegomendiola.android.preguntasCliente.helpers.*;

public class MainDispatcher extends RestActivity {

	private static String url = Configuracion.SERVIDOR + "login/";
	private static final String TAG = MainDispatcher.class.getName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.e(TAG, "On Create");
		this.checkInternet();
		this.checkLoggedIn();
	}

	private void checkInternet() {
		ConnectionDetector conDet = new ConnectionDetector(this);
		if (!(conDet.isConnectingToInternet())) {
			Toast.makeText(this, "�No hay internet!", Toast.LENGTH_LONG).show();
			this.finish();
		}
	}

	private void checkLoggedIn() {
		Log.e(TAG, "LLego a check logged in");
		if (this.savedPreferences()) {
			Log.e(TAG, "Preferencias guardadas.");
			Intent restRequest = new Intent(this, RestClient.class);
			restRequest.setData(Uri.parse(url));
			restRequest.putExtra(RestClient.EXTRA_RESULT_RECEIVER,
					getResultReceiver());
			Log.e(TAG, "Arrancando consulta.");
			startService(restRequest);
		} else {
			Log.e(TAG, "Preferencias no guardadas");
			this.startLoginActivity(null);
		}
	}

	private boolean savedPreferences() {
		Context context = getApplicationContext();
		SharedPreferences sharedPref = context.getSharedPreferences(
				Configuracion.PREFERENCES, Context.MODE_PRIVATE);
		String username = sharedPref.getString("username", "");
		String password = sharedPref.getString("password", "");

		if ((username == "") || (password == "")) {
			return false;
		} else {
			return true;
		}
	}

	private void startLoginActivity(String message) {
		Intent loginIntent = new Intent(this, LoginActivity.class);
		if (message != null) {
			loginIntent.putExtra(Configuracion.ACTIVITY_MESSAGE, message);
		}
		startActivity(loginIntent);
	}

	@Override
	public void onRESTResult(int code, String result) {
		if (code == 200) {
			Intent preguntasIntent = new Intent(this,
					LectorPreguntasActivity.class);
			startActivity(preguntasIntent);
		} else if (code == 413 || code == 401 || code == 403) {
			this.startLoginActivity("Usuario/Contrase�a inv�lidos!");
		} else {
			Toast.makeText(this, "Error de conexi�n con el servidor",
					Toast.LENGTH_LONG).show();
			this.finish();
		}

	}
}
