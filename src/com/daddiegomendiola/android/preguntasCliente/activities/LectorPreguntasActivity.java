/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daddiegomendiola.android.preguntasCliente.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.content.IntentCompat;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.daddiegomendiola.android.preguntasCliente.Configuracion;
import com.daddiegomendiola.android.preguntasCliente.MainDispatcher;
import com.daddiegomendiola.android.preguntasCliente.R;
import com.daddiegomendiola.android.preguntasCliente.fragments.PreguntaFragment;
import com.daddiegomendiola.android.preguntasCliente.fragments.TitulosFragment;

/**
 * Main activity: shows headlines list and articles, if layout permits.
 * 
 * This is the main activity of the application. It can have several different
 * layouts depending on the SDK version, screen size and orientation. The
 * configurations are divided in two large groups: single-pane layouts and
 * dual-pane layouts.
 * 
 * In single-pane mode, this activity shows a list of headlines using a
 * {@link TitulosFragment}. When the user clicks on a headline, a separate
 * activity (a {@link PreguntaActivity}) is launched to show the news article.
 * 
 * In dual-pane mode, this activity shows a {@HeadlinesFragment
 * } on the left side and an {@ArticleFragment
 * } on the right side. When the user selects a headline on the
 * left, the corresponding article is shown on the right.
 * 
 * If an Action Bar is available (large enough screen and SDK version 11 or up),
 * navigation controls are shown in the Action Bar (whether to show tabs or a
 * list depends on the layout). If an Action Bar is not available, a regular
 * image and button are shown in the top area of the screen, emulating an Action
 * Bar.
 */
public class LectorPreguntasActivity extends ActionBarActivity implements
		TitulosFragment.OnHeadlineSelectedListener {

	// Whether or not we are in dual-pane mode
	boolean mIsDualPane = false;

	// The fragment where the headlines are displayed
	TitulosFragment titulosFragment;

	// The fragment where the article is displayed (null if absent)
	PreguntaFragment preguntaFragment;

	// The news category and article index currently being displayed
	// A negative under
	int preguntaIndex = TitulosFragment.NO_TITLE_SELECTED;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_layout);
		this.setTitle(R.string.app_name);

		// find our fragments
		titulosFragment = (TitulosFragment) getSupportFragmentManager()
				.findFragmentById(R.id.titulos);
		preguntaFragment = (PreguntaFragment) getSupportFragmentManager()
				.findFragmentById(R.id.pregunta);

		// Determine whether we are in single-pane or dual-pane mode by testing
		// the visibility
		// of the article view.
		View preguntaView = findViewById(R.id.pregunta);
		mIsDualPane = preguntaView != null
				&& preguntaView.getVisibility() == View.VISIBLE;

		// Register ourselves as the listener for the headlines fragment events.
		titulosFragment.setOnHeadlineSelectedListener(this);

		setUpActionBar();

		// Set up headlines fragment
		titulosFragment.setSelectable(mIsDualPane);
		restoreSelection(savedInstanceState);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.lector_preguntas, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.item_menu_refrescar:
			Intent refrescar = getIntent();
			refrescar.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			finish();
			startActivity(refrescar);
			return true;

		case R.id.item_menu_crear_pregunta:
			Intent intent = new Intent(this, CrearPreguntaActivity.class);
			startActivity(intent);
			return true;

		case R.id.item_menu_logout:
			this.removeLoginInfo();
			this.showMainDispatcher();
			return true;

		default:
			return super.onOptionsItemSelected(item);
		}
	}

	/** Restore category/article selection from saved state. */
	void restoreSelection(Bundle savedInstanceState) {
		if (savedInstanceState != null) {
			if (mIsDualPane) {
				preguntaIndex = savedInstanceState.getInt(
						Configuracion.PREGUNTA_INDEX,
						TitulosFragment.NO_TITLE_SELECTED);
			}
		}
	}

	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		restoreSelection(savedInstanceState);
	}

	public void onTitulosCargados(int index) {
		if (preguntaIndex == TitulosFragment.NO_TITLE_SELECTED) {
			preguntaIndex = index;
		}
		if (mIsDualPane) {
			titulosFragment.setSelection(preguntaIndex);
			onHeadlineSelected(preguntaIndex);
		}
	}

	/**
	 * Sets up Action Bar (if present).
	 * 
	 * @param showTabs
	 *            whether to show tabs (if false, will show list).
	 * @param selTab
	 *            the selected tab or list item.
	 */
	public void setUpActionBar() {
		// ActionBar actionBar = getSupportActionBar();
		// actionBar.setDisplayShowTitleEnabled(false);
		// Show logo instead of icon+title.
		// actionBar.setDisplayUseLogoEnabled(false);
	}

	@Override
	public void onStart() {
		super.onStart();
	}

	/**
	 * Called when a headline is selected.
	 * 
	 * This is called by the HeadlinesFragment (via its listener interface) to
	 * notify us that a headline was selected in the Action Bar. The way we
	 * react depends on whether we are in single or dual-pane mode. In
	 * single-pane mode, we launch a new activity to display the selected
	 * article; in dual-pane mode we simply display it on the article fragment.
	 * 
	 * @param index
	 *            the index of the selected headline.
	 */
	@Override
	public void onHeadlineSelected(int index) {
		preguntaIndex = index;
		if (mIsDualPane) {
			// display it on the article fragment
			preguntaFragment.mostrarPregunta(index);
		} else {
			// use separate activity
			Intent i = new Intent(this, PreguntaActivity.class);
			i.putExtra(Configuracion.PREGUNTA_INDEX, index);
			startActivity(i);
		}
	}

	private void removeLoginInfo() {
		Context context = getApplicationContext();
		SharedPreferences sharedPref = context.getSharedPreferences(
				Configuracion.PREFERENCES, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.clear();
		editor.commit();
	}

	private void showMainDispatcher() {
		Intent main = new Intent(this, MainDispatcher.class);
		main.setFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
		startActivity(main);
		this.finish();
	}

	/** Save instance state. Saves current category/article index. */
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		outState.putInt(Configuracion.PREGUNTA_INDEX, preguntaIndex);
		super.onSaveInstanceState(outState);
	}
}
