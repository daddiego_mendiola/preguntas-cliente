/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.daddiegomendiola.android.preguntasCliente.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.daddiegomendiola.android.preguntasCliente.Configuracion;
import com.daddiegomendiola.android.preguntasCliente.R;
import com.daddiegomendiola.android.preguntasCliente.fragments.PreguntaFragment;

/**
 * Activity that displays a particular news article onscreen.
 * 
 * This activity is started only when the screen is not large enough for a
 * two-pane layout, in which case this separate activity is shown in order to
 * display the news article. This activity kills itself if the display is
 * reconfigured into a shape that allows a two-pane layout, since in that case
 * the news article will be displayed by the {@link LectorPreguntasActivity} and
 * this Activity therefore becomes unnecessary.
 */
public class PreguntaActivity extends ActionBarActivity {
	// The news category index and the article index for the article we are to
	// display
	int preguntaIndex;
	PreguntaFragment preguntaFragment;

	/**
	 * Sets up the activity.
	 * 
	 * Setting up the activity means reading the category/article index from the
	 * Intent that fired this Activity and loading it onto the UI. We also
	 * detect if there has been a screen configuration change (in particular, a
	 * rotation) that makes this activity unnecessary, in which case we do the
	 * honorable thing and get out of the way.
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pregunta);
		preguntaFragment = (PreguntaFragment) getSupportFragmentManager()
				.findFragmentById(R.id.pregunta);

		preguntaIndex = getIntent().getExtras().getInt(
				Configuracion.PREGUNTA_INDEX, 1);

		// If we are in two-pane layout mode, this activity is no longer
		// necessary
		if (getResources().getBoolean(R.bool.has_two_panes)) {
			finish();
			return;
		}

		// Display the correct news article on the fragment
		preguntaFragment.mostrarPregunta(preguntaIndex);
		// getSupportActionBar().setDisplayUseLogoEnabled(false);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.pregunta, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case R.id.item_menu_refrescar:
			Intent refrescar = getIntent();
			refrescar.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
			finish();
			startActivity(refrescar);
			return true;

		case android.R.id.home:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}
	}
}
