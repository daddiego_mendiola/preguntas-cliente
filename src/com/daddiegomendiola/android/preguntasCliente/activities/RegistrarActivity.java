package com.daddiegomendiola.android.preguntasCliente.activities;

import java.util.ArrayList;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.daddiegomendiola.android.preguntasCliente.Configuracion;
import com.daddiegomendiola.android.preguntasCliente.MainDispatcher;
import com.daddiegomendiola.android.preguntasCliente.R;
import com.daddiegomendiola.android.preguntasCliente.restClient.RestClient;

public class RegistrarActivity extends RestActivity {

	private final String url = Configuracion.SERVIDOR + "alumnos/";
	private final String urlSpinner = Configuracion.SERVIDOR + "carreras/";
	Map<String, EditText> mapEditText = new ArrayMap<String, EditText>();
	private static final String TAG = RegistrarActivity.class.getName();
	private Spinner spinnerCarreras;
	ArrayAdapter<String> spinnerAdapter;
	ArrayList<String> nombreCarreras = new ArrayList<String>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registrar);
		mapEditText
				.put("username", (EditText) findViewById(R.id.campo_usuario));
		mapEditText.put("first_name",
				(EditText) findViewById(R.id.campo_nombre));
		mapEditText.put("last_name",
				(EditText) findViewById(R.id.campo_apellido));
		mapEditText.put("email", (EditText) findViewById(R.id.campo_email));
		mapEditText.put("dni", (EditText) findViewById(R.id.campo_dni));
		mapEditText.put("password", (EditText) findViewById(R.id.campo_pass));
		mapEditText.put("password_confirm",
				(EditText) findViewById(R.id.campo_pass_confirm));

		this.spinnerCarreras = (Spinner) findViewById(R.id.campo_carrera);
		this.requestSpinnerCarreras();

		Log.e(TAG, "Map cargado.");
	}

	private void requestSpinnerCarreras() {
		Intent restRequest = new Intent(this, RestClient.class);
		restRequest.setData(Uri.parse(this.urlSpinner));
		restRequest.putExtra(RestClient.EXTRA_RESULT_RECEIVER,
				this.getResultReceiver());
		restRequest.putExtra(RestClient.EXTRA_HTTP_AUTH, false);
		startService(restRequest);
	}

	public void botonRegistrar(View view) {
		boolean camposVacios = false;
		for (Map.Entry<String, EditText> entry : mapEditText.entrySet()) {
			String campo = entry.getValue().getText().toString().trim();
			Log.e(TAG, "Recorriendo map: " + entry.getKey() + ": " + campo);
			if (campo.length() == 0) {
				int id = R.string.field;
				try {
					id = R.string.class.getField(entry.getKey()).getInt(null);
				} catch (Exception e) {

				}
				Log.e(TAG, String.valueOf(id));
				String campo_vacio = getString(id);
				Toast.makeText(this, campo_vacio + ": Vac�o.",
						Toast.LENGTH_SHORT).show();
				entry.getValue().requestFocus();
				camposVacios = true;
				break;
			}
		}
		if (!camposVacios) {
			Log.e(TAG, "campos completos. Comprobando password.");
			String pass = mapEditText.get("password").getText().toString();
			String pass_confirm = mapEditText.get("password_confirm").getText()
					.toString();
			if (pass.equals(pass_confirm)) {
				Log.e(TAG, "Passwords ok. Enviando consulta.");
				this.registrar();
			} else {
				Toast.makeText(this, "Las contrase�as no coinciden",
						Toast.LENGTH_SHORT).show();
			}

		}
	}

	private void registrar() {
		Intent restRequest = new Intent(this, RestClient.class);
		restRequest.setData(Uri.parse(this.url));
		restRequest.putExtra(RestClient.EXTRA_HTTP_VERB, RestClient.POST);
		restRequest.putExtra(RestClient.EXTRA_HTTP_AUTH, false);
		restRequest.putExtra(RestClient.EXTRA_RESULT_RECEIVER,
				this.getResultReceiver());
		Bundle params = new Bundle();
		for (Map.Entry<String, EditText> entry : mapEditText.entrySet()) {
			if (!entry.getKey().equals("password_confirm")) {
				params.putString(entry.getKey(), entry.getValue().getText()
						.toString());
			}
		}
		params.putString("carrera", this.nombreCarreras
				.get(this.spinnerCarreras.getSelectedItemPosition()));
		restRequest.putExtra(RestClient.EXTRA_PARAMS, params);
		this.startService(restRequest);
	}

	private void saveLoginData() {
		Context context = getApplicationContext();
		SharedPreferences sharedPref = context.getSharedPreferences(
				Configuracion.PREFERENCES, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("username", this.mapEditText.get("username").getText()
				.toString());
		editor.putString("password", this.mapEditText.get("password").getText()
				.toString());
		editor.commit();
	}

	private void loadSpinnerCarreras(String result) {
		try {

			JSONArray carreras = new JSONArray(result);
			for (int i = 0; i < carreras.length(); i++) {
				JSONObject carrera = carreras.getJSONObject(i);
				this.nombreCarreras.add(carrera.getString("nombre"));
			}
		} catch (Exception e) {

		}

		this.spinnerAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, nombreCarreras);
		this.spinnerAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		this.spinnerCarreras.setAdapter(this.spinnerAdapter);
		this.spinnerCarreras.setSelection(0);
	}
	
	private void mostrarError(String result){
		try {
			JSONObject obj = new JSONObject(result);
			String key = (String) obj.keys().next();
			result = obj.getJSONArray(key).getString(0);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Toast.makeText(this, result, Toast.LENGTH_LONG).show();
	}

	@Override
	public void onRESTResult(int code, String result) {
		switch (code) {
		case 200:
			loadSpinnerCarreras(result);
			break;
		case 201:
			saveLoginData();
			Intent main = new Intent(this, MainDispatcher.class);
			startActivity(main);
			break;

		default:
			mostrarError(result);
			Log.e(TAG, "Codigo: " + String.valueOf(code) + "\n. Result: "
					+ result);
			break;
		}

	}
}
