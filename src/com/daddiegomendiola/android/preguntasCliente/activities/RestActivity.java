package com.daddiegomendiola.android.preguntasCliente.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;
import android.support.v7.app.ActionBarActivity;

import com.daddiegomendiola.android.preguntasCliente.restClient.RestClient;


public abstract class RestActivity extends ActionBarActivity {

	private ResultReceiver mReceiver;

	public RestActivity() {
		mReceiver = new ResultReceiver(new Handler()) {
			@Override
			protected void onReceiveResult(int resultCode, Bundle resultData) {
				if (resultData != null
						&& resultData.containsKey(RestClient.REST_RESULT)) {
					onRESTResult(resultCode,
							resultData.getString(RestClient.REST_RESULT));
				} else {
					onRESTResult(resultCode, null);
				}
			}
		};
	}

	public ResultReceiver getResultReceiver() {
		return mReceiver;
	}

	// Implementers of this Fragment will handle the result here.
	abstract public void onRESTResult(int code, String result);
}