package com.daddiegomendiola.android.preguntasCliente.activities;

import org.json.JSONObject;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.daddiegomendiola.android.preguntasCliente.Configuracion;
import com.daddiegomendiola.android.preguntasCliente.restClient.RestClient;
import com.daddiegomendiola.android.preguntasCliente.views.PerfilWebView;

public class VerPerfilActivity extends RestActivity {
	private static final String url = Configuracion.SERVIDOR + "alumnos/";
	private static final String TAG = VerPerfilActivity.class.getName();
	int alumnoIndex;
	PerfilWebView perfilWebView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle("Cargando perfil...");
		this.perfilWebView = new PerfilWebView(this);
		this.setContentView(this.perfilWebView);
		int index = getIntent().getExtras().getInt(Configuracion.ALUMNO_INDEX,
				1);
		this.cargarPerfil(index);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	}

	private void cargarPerfil(int index) {
		Intent restRequest = new Intent(this, RestClient.class);
		String urlRespuesta = url + String.valueOf(index) + "/";
		restRequest.setData(Uri.parse(urlRespuesta));
		restRequest.putExtra(RestClient.EXTRA_RESULT_RECEIVER,
				this.getResultReceiver());
		startService(restRequest);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	private void parsearPerfil(String jsonS) {
		try {
			JSONObject alumno = new JSONObject(jsonS);
			setTitle("Perfil de: " + alumno.getString("username"));
			this.perfilWebView.procesarPerfil(alumno);
		} catch (Exception e) {
			Log.e(TAG, e.getMessage());
		}
	}

	@Override
	public void onRESTResult(int code, String result) {
		switch (code) {
		case 200:
			this.parsearPerfil(result);
			break;

		default:
			break;
		}

	}
}
