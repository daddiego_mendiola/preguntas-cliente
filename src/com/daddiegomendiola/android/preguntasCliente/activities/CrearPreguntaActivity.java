package com.daddiegomendiola.android.preguntasCliente.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import com.daddiegomendiola.android.preguntasCliente.Configuracion;
import com.daddiegomendiola.android.preguntasCliente.restClient.RestClient;
import com.daddiegomendiola.android.preguntasCliente.views.CrearPreguntaWebView;

public class CrearPreguntaActivity extends RestActivity {

	private CrearPreguntaWebView crearPreguntaWebView;
	private final String url = Configuracion.SERVIDOR + "preguntas/";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.setTitle("Hacer una pregunta");
		this.crearPreguntaWebView = new CrearPreguntaWebView(this, this);
		this.setContentView(this.crearPreguntaWebView);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		switch (id) {
		case android.R.id.home:
			onBackPressed();
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void preguntar(String titulo, String descripcion) {
		Intent restRequest = new Intent(this, RestClient.class);
		restRequest.setData(Uri.parse(this.url));
		restRequest.putExtra(RestClient.EXTRA_HTTP_VERB, RestClient.POST);
		restRequest.putExtra(RestClient.EXTRA_RESULT_RECEIVER,
				this.getResultReceiver());
		Bundle params = new Bundle();
		params.putString("titulo", titulo);
		params.putString("descripcion", descripcion);
		restRequest.putExtra(RestClient.EXTRA_PARAMS, params);
		this.startService(restRequest);
	}

	public void toast(String msg) {
		Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
	}

	@Override
	public void onRESTResult(int code, String result) {
		switch (code) {
		case 201:
			finish();
			break;
		case 400:
			Toast.makeText(this, "�Faltan campos!", Toast.LENGTH_SHORT).show();
			break;
		default:
			Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
			break;
		}
	}
}