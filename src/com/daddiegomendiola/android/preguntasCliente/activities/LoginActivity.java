package com.daddiegomendiola.android.preguntasCliente.activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.daddiegomendiola.android.preguntasCliente.Configuracion;
import com.daddiegomendiola.android.preguntasCliente.MainDispatcher;
import com.daddiegomendiola.android.preguntasCliente.R;

public class LoginActivity extends ActionBarActivity {

	EditText campo_usuario;
	EditText campo_pass;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		this.showMessage();
		this.campo_usuario = (EditText) findViewById(R.id.campo_usuario);
		this.campo_pass = (EditText) findViewById(R.id.campo_pass);
	}

	private void saveLoginInfo() {
		Context context = getApplicationContext();
		SharedPreferences sharedPref = context.getSharedPreferences(
				Configuracion.PREFERENCES, Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = sharedPref.edit();
		editor.putString("username", this.campo_usuario.getText().toString());
		editor.putString("password", this.campo_pass.getText().toString());
		editor.commit();
	}
	
	private void showMessage(){
		String message = getIntent().getStringExtra(Configuracion.ACTIVITY_MESSAGE);
		getIntent().removeExtra(Configuracion.ACTIVITY_MESSAGE);
		if (message != null)
			Toast.makeText(this, message, Toast.LENGTH_LONG).show();
	}
	
	private void goToDispatcher(){
		Intent dispt = new Intent(this, MainDispatcher.class);
		startActivity(dispt);
	}
	
	private void goToRegister(){
		Intent reg = new Intent(this, RegistrarActivity.class);
		startActivity(reg);
	}

	public void botonLogin(View view) {
		this.saveLoginInfo();
		this.goToDispatcher();
	}

	public void botonRegistrar(View view) {
		this.goToRegister();
	}
}