package com.daddiegomendiola.android.preguntasCliente.views;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.webkit.JavascriptInterface;

import com.daddiegomendiola.android.preguntasCliente.fragments.PreguntaFragment;
import com.daddiegomendiola.android.preguntasCliente.helpers.DateFormatManager;
import com.x5.template.Chunk;

@SuppressLint("SetJavaScriptEnabled")
public class PreguntaWebView extends CustomWebView {
	private PreguntaFragment parent;
	private static final String TAG = PreguntaWebView.class.getName();

	public PreguntaWebView(Context context, PreguntaFragment parent) {
		super(context);
		this.parent = parent;
	}

	public void procesarPregunta(JSONObject pregunta, JSONObject alumno)
			throws Exception {
		Chunk template = theme.makeChunk("preguntaWebView/pregunta");
		template.set("titulo", pregunta.getString("titulo"));
		template.set("usuario", alumno.getString("username"));
		template.set("usuario_id", alumno.getString("id"));
		template.set("fecha",
				DateFormatManager.correctTimeZone(pregunta.getString("fecha")).toString());
		template.set("descripcion", pregunta.get("descripcion"));
		this.appendEntity(template.toString());
	}

	public void procesarRespuesta(JSONObject respuesta, JSONObject alumno)
			throws Exception {
		Chunk template = theme.makeChunk("preguntaWebView/respuesta");
		template.set("descripcion", respuesta.getString("descripcion"));
		template.set("usuario", alumno.getString("username"));
		template.set("usuario_id", alumno.getString("id"));
		template.set("fecha",
				DateFormatManager.correctTimeZone(respuesta.getString("fecha")).toString());
		this.appendEntity(template.toString());
	}

	public void crearFormularioRespuesta() {
		Chunk template = theme.makeChunk("preguntaWebView/responder");
		this.appendEntity(template.toString());
	}

	@JavascriptInterface
	public void responder(String respuesta) {
		parent.responder(respuesta);
	}
	
	@JavascriptInterface
	public void perfil(String index){
		Log.e(TAG, "Llego ac� al metodo perfil de webview. ID: " + index);
		parent.perfil(Integer.parseInt(index));
	}
}