package com.daddiegomendiola.android.preguntasCliente.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.webkit.JavascriptInterface;

import com.daddiegomendiola.android.preguntasCliente.activities.CrearPreguntaActivity;
import com.x5.template.Chunk;

@SuppressLint("SetJavaScriptEnabled")
public class CrearPreguntaWebView extends CustomWebView {
	
	private CrearPreguntaActivity parent;
	
	public CrearPreguntaWebView(Context context, CrearPreguntaActivity parent) {
		super(context);
		this.parent = parent;
		this.load();
		this.showData();
	}
	
	private void load(){
		Chunk template = theme.makeChunk("crearPreguntaWebView/preguntar");
		this.appendEntity(template.toString());
	}

	@JavascriptInterface
	public void preguntar(String titulo, String descripcion) {
		parent.preguntar(titulo, descripcion);
	}
	
	@JavascriptInterface
	public void toast(String msg){
		parent.toast(msg);
	}
}
