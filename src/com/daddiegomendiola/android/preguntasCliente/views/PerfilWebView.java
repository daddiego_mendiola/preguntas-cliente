package com.daddiegomendiola.android.preguntasCliente.views;

import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;

import com.x5.template.Chunk;

@SuppressLint("SetJavaScriptEnabled")
public class PerfilWebView extends CustomWebView {

	public PerfilWebView(Context context) {
		super(context);
	}

	public void procesarPerfil(JSONObject alumno) throws Exception {
		Chunk template = theme.makeChunk("perfilWebView/verPerfil");
		template.set("username", alumno.getString("username"));
		template.set("apellido", alumno.getString("last_name"));
		template.set("nombre", alumno.getString("first_name"));
		template.set("mail", alumno.getString("email"));
		template.set("carrera", alumno.getString("carrera"));
		this.appendEntity(template.toString());
		this.showData();
	}
}