package com.daddiegomendiola.android.preguntasCliente.views;

import android.annotation.SuppressLint;
import android.content.Context;
import android.webkit.WebView;

import com.daddiegomendiola.android.preguntasCliente.activities.CrearPreguntaActivity;
import com.x5.template.Chunk;
import com.x5.template.Theme;
import com.x5.template.providers.AndroidTemplates;

@SuppressLint("SetJavaScriptEnabled")
public class CustomWebView extends WebView {

	protected final String javascriptInterface = "Preguntas";

	StringBuilder data;
	protected AndroidTemplates loader;
	protected Theme theme;
	protected CrearPreguntaActivity parent;

	public CustomWebView(Context context) {
		super(context);
		this.loader = new AndroidTemplates(context);
		this.theme = new Theme(loader);
		this.init();
	}

	public void init() {
		Chunk template = theme.makeChunk("loading");
		this.loadData(template.toString(), "text/html", "utf-8");
		this.data = new StringBuilder();
		this.setJavascript();
	}

	protected void setJavascript() {
		this.getSettings().setJavaScriptEnabled(true);
		this.addJavascriptInterface(this, javascriptInterface);
	}

	protected void appendEntity(String html) {
		this.data.append(html);
	}

	public void showData() {
		Chunk template = theme.makeChunk("base");
		template.set("contenido", this.data.toString());
		this.loadDataWithBaseURL("file:///android_asset/", template.toString(),
				"text/html", "utf-8", null);
	}
}
