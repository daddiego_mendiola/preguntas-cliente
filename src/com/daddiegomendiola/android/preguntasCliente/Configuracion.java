package com.daddiegomendiola.android.preguntasCliente;

public class Configuracion {
	public static final String SERVIDOR = "https://daddiego.com.ar/preguntas/api/";
	public static final String PREFERENCES = "com.daddiegomendiola.android.preguntasCliente.preferences";
	public static final String ACTIVITY_MESSAGE = "com.daddiegomendiola.android.preguntasCliente.activity.message";
	public static final String PREGUNTA_INDEX = "com.daddiegomendiola.android.preguntasCliente.preguntaIndex";
	public static final String ALUMNO_INDEX = "com.daddiegomendiola.android.preguntasCliente.alumnoIndex";
	public static final int REQUEST_CODE = 1;
}
