package com.daddiegomendiola.android.preguntasCliente.helpers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import android.annotation.SuppressLint;

public final class DateFormatManager {

	@SuppressLint("SimpleDateFormat")
	public static String correctTimeZone(String date) {
		String result = "";
		try {
			SimpleDateFormat sourceFormat = new SimpleDateFormat(
					"dd-MM-yyyy HH:mm");
			sourceFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			Date parsed = sourceFormat.parse(date);

			TimeZone tz = TimeZone.getDefault();
			SimpleDateFormat destFormat = new SimpleDateFormat(
					"dd-MM-yyyy HH:mm");
			destFormat.setTimeZone(tz);

			result = destFormat.format(parsed);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return result;
	}
}
